<?php

namespace App\Http\Controllers;

use App\Models\BaseRow;
use App\Models\JackpotType;
use App\Models\PrizeType;

use Illuminate\Http\Request;
use Carbon\Carbon;

class AnalyticController extends BaseController
{
   
    public function __construct()
    {
        parent::__construct();
    }

    public function number(Request $request)
    {
    	$number = $request->get('number', '');
    	$jackpot_type_id = $request->get('jackpot_type_id', '');
    	$prize_type_id = $request->get('prize_type_id', '');
    	$start_date = $request->get('start_date', '');
        $end_date = $request->get('end_date', '');
        $limit = $request->get('limit', 20);
        $order_by = $request->get('order_by', 'desc');

        $query = BaseRow::query();

        if (!empty($number)) {
            $query->where('number', $number);
        }
        if (!empty($jackpot_type_id)) {
            $query->where('jackpot_type_id', $jackpot_type_id);
        }
        if (!empty($prize_type_id)) {
            $query->where('prize_type_id', $prize_type_id);
        }
        if (!empty($start_date)) {
            $query->whereDate('date', '>=', Carbon::parse($start_date)->format('Y-m-d 00:00:00'));
        }
        if (!empty($end_date)) {
            $query->whereDate('date', '<=', Carbon::parse($end_date)->format('Y-m-d 23:59:59'));
        }

        if (!empty($order_by)) {
            $query->orderBy('id', $order_by);

        }

        $number_list  = $this->my_function->manageNumber($query->paginate($limit));
        $number_total  = $query->count();

        $jackpot_type_list = JackpotType::get();
        $jackpot_type_list = $this->my_function->selectStructure($jackpot_type_list, 'id', 'name', 'All');

        $prize_type_list = PrizeType::get();
        $prize_type_list = $this->my_function->selectStructure($prize_type_list, 'id', 'name', 'All');

        return view('analytic.number', compact('number_list', 'number_total', 'jackpot_type_list', 'prize_type_list'));
    }
}
