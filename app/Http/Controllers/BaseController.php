<?php

namespace App\Http\Controllers;

use App\Http\Libraries\JackPot_SDK;
use App\Http\Helpers\MyFunctions;

class BaseController extends Controller
{
    protected $jackpot;
    protected $my_function;

    public function __construct()
    {
        $this->middleware('auth');
        $this->jackpot = new JackPot_SDK();
        $this->my_function = new MyFunctions();
    }
}