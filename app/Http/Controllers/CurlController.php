<?php

namespace App\Http\Controllers;

use App\Models\BaseRow;
use Carbon\Carbon;

use App\Jobs\baseInsertPatch;
use App\Jobs\lastDrawUpdate;

use Illuminate\Http\Request;

use Carbon\CarbonPeriod;

use Illuminate\Support\Facades\DB;

class CurlController extends BaseController
{
   
    public function __construct()
    {
        parent::__construct();
    }

    public function base()
    {
        // $period = CarbonPeriod::create('2018-12-25', '2018-12-26');

        // foreach ($period as $key_period => $row_period) {
        //     $curl_date = $row_period->format('Y-m-d');
        //     baseInsertPatch::dispatch($curl_date);
        // }
    }

    public function last()
    {
        // $base = BaseRow::where('last_draw_id', '!=', null)->update(['last_draw_id' => null, 'last_draw_date' => null, 'last_draw_same_type_id' => null, 'last_draw_same_type_date' => null]);
        lastDrawUpdate::dispatch();
    }
}
