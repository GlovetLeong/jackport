<?php

namespace App\Http\Controllers;

use Auth;
use App\User;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use Illuminate\Http\Request;
use App\Http\Requests\UserRequest;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class UserController extends BaseController
{
   
    public function __construct()
    {
        parent::__construct();
    }

    public function index(Request $request)
    {
    	$name = $request->get('name', '');
        $email = $request->get('email', '');
        $status = $request->get('status', '');

        $query = User::query();
        if (!empty($name)) {
            $query->where('name', 'like', '%' . $name . '%');
        }
        if (!empty($email)) {
            $query->where('name', 'like', '%' . $name . '%');
        }
        if (strlen($status) > 0) {
            $query->where('status', $status);
        }

        $user_list = $query->paginate(10);
        $user_total = $query->count();

        return view('user.index', compact('user_list', 'user_total'));
    }

    public function create()
    {
    	$role_list = $this->getRoleList();
        $role_list = $this->my_function->selectStructure($role_list, 'id', 'name');

        return view('user.create', compact('role_list'));
    }

    public function store(UserRequest $request)
    {
    	DB::beginTransaction();

        $user = new User();

        $user->name = $request->input('name');
        $user->email = $request->input('email');
        $user->password = $request->input('password');
        $user->status = $request->input('status', 0);

        $user->save();

        $user->syncRoles($request->input('user_role'));

        DB::commit();
        
        return redirect()->route('user')->withSuccess('User (' . $user->name . ') has been created.');
    }

    public function edit($id)
    {
    	$this_user = $this->show($id);

        $role_list = $this->getRoleList();
        $role_list = $this->my_function->selectStructure($role_list, 'id', 'name');

        return view('user.edit', compact('this_user', 'role_list'));
    }

    public function update(UserRequest $request)
    {
    	DB::beginTransaction();

        $id = $request->input('id');
        $user = User::lockForUpdate()->find($id);

        $user->name = $request->input('name', $user->name);
        $user->email = $request->input('email', $user->email);
        if (!empty($request->input('password'))) {
            $user->password = $request->input('password');
        }
        $user->status = $request->input('status', 0);
        $user->updated_at = Carbon::now();
      
        $user->update();
        $user->syncRoles($request->input('user_role'));

        DB::commit();

        return redirect()->route('user')->withSuccess('User (' . $user->name . ') has been updated.');
    }

    public function delete(Request $request)
    {

    }

    public function show($id)
    {
        $user = User::find($id);
        $user->role = $user->getRoleNames();
        $user->role_id = $user->getRoleId();

        return $user;
    }

    public function getRoleList()
    {
        $role_list = [];
        if (Auth::user()->hasRole('Super Admin')) {
            $role_list = Role::where('status', 1)->get();
        }
        else {
            $role_list = Role::where('name', '!=', 'Super Admin')
                ->where('status', 1)
                ->get();
        }
        return $role_list;
    }

}
