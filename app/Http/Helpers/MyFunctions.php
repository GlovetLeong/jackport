<?php
namespace App\Http\Helpers;

use Carbon\Carbon;
use Carbon\CarbonPeriod;

use Cache;

class MyFunctions
{
    
    function __construct()
    {
        # code...
    }

    function manageNumber($data) {
        $now = Carbon::now();
        foreach ($data as $key => $row) {
            $drawn_days_period = CarbonPeriod::create($row->date, $now);
            $row->date = Carbon::parse($row->date)->format('Y-m-d');
            $row->drawn_days = (count($drawn_days_period) - 1) . ' days';

            if (!empty($row->last_draw_id)) {
                $last_drawn_date = $row->getLastDrawn()->date;
                $last_drawn_days_period = CarbonPeriod::create($last_drawn_date, $row->date);
                $row->last_drawn_days = (count($last_drawn_days_period) - 1) . ' days';
            }

            if (!empty($row->last_draw_same_type_id)) {
                $last_drawn_same_type_date = $row->getLastDrawnSameType()->date;
                $last_drawn_same_type_days_period = CarbonPeriod::create($last_drawn_same_type_date, $row->date);
                $row->last_drawn_same_type_days = (count($last_drawn_same_type_days_period) - 1) . ' days';
            }

        }
        return $data;
    }
    
    function getExcerpt($str, $maxLength = 150, $startPos = 0) {
        $str = strip_tags($str);
        $str = html_entity_decode($str, ENT_QUOTES);
        $excerpt = $str;
        if(strlen($str) > $maxLength) {
            $excerpt = '';

            $excerpt = mb_substr($str, $startPos, $maxLength-3, 'UTF-8');

            $excerpt .= '...';
        } 
        return $excerpt;
    }

    function arrayString($set = [], $name = '')
    {
        $string_set = '';
        $array_set = [];
        foreach ($set as $key => $row) {
            $array_set[] = $row->$name;
        }
        $string_set = implode(', ', $array_set);
        return $string_set;
    }

    function checkIsExist($value = '', $set = [])
    {
        if (in_array($value, $set)) {
            return true;
        }
        else {
            return false;
        }
    }

    function selectStructure($set = [], $value = '', $name = '', $default = '-- Select an Option --')
    {
        $return_set = [];
        $x = 0;
        foreach ($set as $key => $row) {
            if ($x == 0) {
                $return_set[''] = $default;
            }
            $return_set[$row->$value] = $row->$name;
            $x++;
        }
        return $return_set;
    }

    function processActive($base_set = [], $active_set = [])
    {
        $return_data = [];
        $x = 0;
        foreach ($base_set as $key_base => $row_base) {
            foreach ($active_set as $key_active => $row_active) {
                if ($key_base == $key_active) {
                    $return_data[] = (object)[
                        'id' => $key_base,
                        'name' => $row_base,
                        'status' => 1,
                    ];
                    $x++;
                    break;
                }
            }
            if($x == 1){
                $x--;
                continue;
            }
            $return_data[] = (object)[
                'id' => $key_base,
                'name' => $row_base,
                'status' => 0,
            ];
        }
        return $return_data;
    }

    function numberFormatShort( $n = 0 ) 
    {
        $n_format = 0;
        $suffix = '';
        if ($n > 0 && $n < 1000) {
            // 1 - 999
            $n_format = floor($n);
            $suffix = '';
        } else if ($n >= 1000 && $n < 1000000) {
            // 1k-999k
            $n_format = floor($n / 1000);
            $suffix = 'K+';
        } else if ($n >= 1000000 && $n < 1000000000) {
            // 1m-999m
            $n_format = floor($n / 1000000);
            $suffix = 'M+';
        } else if ($n >= 1000000000 && $n < 1000000000000) {
            // 1b-999b
            $n_format = floor($n / 1000000000);
            $suffix = 'B+';
        } else if ($n >= 1000000000000) {
            // 1t+
            $n_format = floor($n / 1000000000000);
            $suffix = 'T+';
        }

        return !empty($n_format . $suffix) ? $n_format . $suffix : 0;
    }

    function seoURL($string)
    {
        //Lower case everything
        $string = strtolower($string);
        //Remove all url format string
        $string =preg_replace("/\b(https?|ftp|file):\/\/[-A-Z0-9+&@#\/%?=~_|$!:,.;]*[A-Z0-9+&@#\/%=~_|$]/i", "", $string);
       
        $string = preg_replace("~[^\p{L}\n\s_]+~u", "", $string);
        
        //Clean up multiple dashes or whitespaces
        $string = preg_replace("/[\s-]+/", " ", $string);
        //Trim first - avoid spaces on beginning or end of the text
        $string = trim($string);

        // Check whether is English
        $match = preg_match('/^[A-Za-z0-9 _]+$/', $string);

        $isEnglish = $match == 1;

        if ($isEnglish == 1) {
            $string = mb_substr($string, 0, 60);
            $strArray = explode(" ",$string);
            if (sizeof($strArray) != 1) {
                // unset($strArray[intval(sizeof($strArray))-1]);
            }
            $string = implode(' ',$strArray);

        }else{
            $string = mb_substr($string, 0, 20,'UTF8');
        }

        //Convert whitespaces and underscore to dash
        $string = preg_replace("/[\s_]/", "-", $string);

        //Check empty string
        if (empty($string)) {
            $string = 'untitled';
        }
        return $string;
    }
}