<?php
    function isActiveRoute($route, $output = 'active')
    {
        $routeName = Route::currentRouteName();
        if ((is_string($route) && ($routeName == $route)) ||
            (is_array($route) && (in_array($routeName, $route)))
            ) {
            return $output;
        }
    }

    function isActivePrefix($route, $output = 'active')
    {
        $routeName = Route::current();
        if ($routeName) {
            $routePrefix = str_replace('/', '.',$routeName->getPrefix());

            if (empty($routePrefix)) $routePrefix = $routeName->uri();
            $routePrefix = ltrim($routePrefix, '/');

            if (is_string($route) && stripos($routePrefix, $route) === 0) return $output;
            if (is_array($route)) {
                $active = false;
                foreach ($route as $prefix) {
                    $active = (stripos($routePrefix, $prefix) === 0);
                    if ($active) break;
                }
                if ($active) return $output;
            }
        }
    }

    function isActivePrefixNoDot($route, $output = 'active')
    {
        $routeName = Route::current();
        if ($routeName) {
            $routePrefix = str_replace('/', '',$routeName->getPrefix());

            if (empty($routePrefix)) $routePrefix = $routeName->uri();
            $routePrefix = ltrim($routePrefix, '/');

            if (is_string($route) && stripos($routePrefix, $route) === 0) return $output;
            if (is_array($route)) {
                $active = false;
                foreach ($route as $prefix) {
                    $active = (stripos($routePrefix, $prefix) === 0);
                    if ($active) break;
                }
                if ($active) return $output;
            }
        }
    }