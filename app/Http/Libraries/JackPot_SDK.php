<?php
namespace App\Http\Libraries;

use Curl\Curl;

class JackPot_SDK
{
	private $curl;
	private $date;

	function __construct()
	{
        $this->curl = new Curl();
	}

	function getDailyCurl($date)
	{
		$this->date = $date;
        $result = $this->getApi();
		return $result;
	}

	function getApi()
    {
        $this->request_url = env('4D_CURL_URL') . '?draw_date=' . $this->date;
        $result = $this->curl->get($this->request_url);
        return json_decode($result);
    }
}