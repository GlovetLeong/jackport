<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserRequest extends FormRequest
{
    
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        $this->id = $this->route('user');
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $this->sanitize();

        return [
            'name' => 'required|string|max:255',
            'email' => 'required|email|max:255|unique:users,email,' . $this->id,
            'password' => $this->isMethod('PATCH') ? 'sometimes|confirmed' : 'required|min:6|confirmed',
            'user_role' => 'required',
            'image' => 'image|max:1000'
        ];
    }
    
    public function sanitize()
    {
        $input = $this->all();
        $input['password'] = !empty($input['password']) ? bcrypt($input['password']) : '';
        $this->replace($input);     
    }    
}
