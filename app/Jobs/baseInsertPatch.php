<?php

namespace App\Jobs;

use App\Models\BaseRow;
use App\Http\Libraries\JackPot_SDK;

use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class baseInsertPatch implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    private $date = '';
    
    public function __construct($date = '')
    {
        $this->date = $date;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $this->jackpot = new JackPot_SDK();
        $curl_date = $this->date;

        $result = $this->jackpot->getDailyCurl($curl_date);

        $in = ['M', 'PMP', 'ST'];
        $not = ['dn', 'live', 'dd', 'td'];

        $date = Carbon::parse($curl_date);
        $now = Carbon::now();

        $data = [];

        if (!empty($result)) {

            DB::beginTransaction();

            foreach ($result as $key => $row) {
                if (in_array($row->t, $in) && !in_array($row->p, $not) && $row->n != '----') {

                    if($row->t == 'M') {
                        $jackpot_type_id = 1;
                    }
                    else if($row->t == 'PMP') {
                        $jackpot_type_id = 2;
                    }
                    else if($row->t == 'ST') {
                        $jackpot_type_id = 3;
                    }

                    $data[] = [
                        'jackpot_type_id' => $jackpot_type_id,
                        'prize_type_id' => $row->p,
                        'number' => trim($row->n),
                        'number_1' => trim($row->n[0]),
                        'number_2' => trim($row->n[1]),
                        'number_3' => trim($row->n[2]),
                        'number_4' => trim($row->n[3]),
                        'date' => $date,
                        'created_at' => $now,
                        'updated_at' => $now,
                    ];
                }
            }

            if (!empty($data)) {
                BaseRow::insert($data);
            }

            DB::commit();
        }
    }
}
