<?php

namespace App\Jobs;

use App\Models\BaseRow;
use App\Http\Libraries\JackPot_SDK;

use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class lastDrawUpdate implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    
    public function __construct()
    {

    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $base = BaseRow::where('last_draw_id', null)->limit(50)->orderBy('id', 'DESC')->get();

        if (!empty($base)) {
            foreach ($base as $key => $row) {
                DB::beginTransaction();

                $last_draw = BaseRow::where('number', $row->number)
                                ->where('id', '<', $row->id)
                                ->where('id', '!=', $row->id)
                                ->limit(1)
                                ->orderBy('id', 'DESC')
                                ->get();

                $last_draw_same_type = BaseRow::where('number', $row->number)
                                        ->where('jackpot_type_id', $row->jackpot_type_id)
                                        ->where('id', '<', $row->id)
                                        ->where('id', '!=', $row->id)
                                        ->limit(1)
                                        ->orderBy('id', 'DESC')
                                        ->get();

                if (count($last_draw) > 0) {
                    $row->last_draw_id = $last_draw[0]->id;
                    $row->last_draw_date = $last_draw[0]->date;
                }

                if (count($last_draw_same_type) > 0) {
                    $row->last_draw_same_type_id = $last_draw_same_type[0]->id;
                    $row->last_draw_same_type_date = $last_draw_same_type[0]->date;
                }

                $row->save();

                DB::commit();

              
            }

            lastDrawUpdate::dispatch()
                    ->delay(Carbon::now()->addSeconds(10));
        }
    }
}
