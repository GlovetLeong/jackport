<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BaseRow extends Model
{
    protected $table = 'base_rows';

    public function jackpot_type()
    {
        return $this->belongsTo('App\Models\JackpotType', 'jackpot_type_id');
    }

    public function prize_type()
    {
        return $this->belongsTo('App\Models\PrizeType', 'prize_type_id');
    }

    public function last_drawn()
    {
        return $this->belongsTo('App\Models\BaseRow', 'last_draw_id');
    }

    public function last_drawn_same_type()
    {
        return $this->belongsTo('App\Models\BaseRow', 'last_draw_same_type_id');
    }

    public function getJackpotType()
    {
        $jackpot_type = $this->jackpot_type()->first();
        return $jackpot_type;
    }

    public function getPrizeType()
    {
        $prize_type = $this->prize_type()->first();
        return $prize_type;
    }

    public function getLastDrawn()
    {
        $last_drawn = $this->last_drawn()->first();
        return $last_drawn;
    }

    public function getLastDrawnSameType()
    {
        $last_drawn_same_type = $this->last_drawn_same_type()->first();
        return $last_drawn_same_type;
    }
}
