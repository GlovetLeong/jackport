<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Spatie\Permission\Traits\HasRoles;

class User extends Authenticatable
{
    use 
        Notifiable,
        HasRoles;

    protected $table = 'users';

    protected $guard = 'web';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    function getRoleId()
    {
        $role_list = $this->roles()->pluck('id');
        return $role_list;
    }

    function getRoleName()
    {
        $role_list = $this->getRoleNames();
        $role = [];

        foreach ($role_list as $key => $row) {
            $role[] = $row;
        }

        return $role = !empty($role) ? implode(', ', $role) : '-';
    }
}
