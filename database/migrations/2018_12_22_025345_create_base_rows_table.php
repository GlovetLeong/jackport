<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBaseRowsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('base_rows', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('jackpot_type_id')->unsigned();
            $table->foreign('jackpot_type_id')->references('id')->on('jackpot_types');
            $table->integer('prize_type_id')->unsigned();
            $table->foreign('prize_type_id')->references('id')->on('prize_types');
            $table->string('number');
            $table->string('number_1');
            $table->string('number_2');
            $table->string('number_3');
            $table->string('number_4');
            $table->integer('status')->default(9);
            $table->timestamp('date')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('base_rows');
    }
}
