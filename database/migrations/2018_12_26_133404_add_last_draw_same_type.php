<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddLastDrawSameType extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('base_rows', function (Blueprint $table) {
            $table->timestamp('last_draw_same_type_date')->nullable();
            $table->integer('last_draw_same_type_id')->unsigned()->nullable();
            $table->foreign('last_draw_same_type_id')->references('id')->on('base_rows');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('base_rows', function (Blueprint $table) {
            //
        });
    }
}
