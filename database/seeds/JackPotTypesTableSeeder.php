<?php

use Illuminate\Database\Seeder;

class JackPotTypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('jackpot_types')->insert([
            [
                'name' => 'magnum'
            ],
            [
                'name' => 'damacai'
            ],
            [
                'name' => 'toto'
            ]
        ]);
    }
}
