<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

use App\User;

class PermissionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Reset cached roles and permissions
        app()['cache']->forget('spatie.permission.cache');

        // create permissions
        Permission::create(['name' => 'view user', 'guard_name' => 'web']);
        Permission::create(['name' => 'add user', 'guard_name' => 'web']);
        Permission::create(['name' => 'edit user', 'guard_name' => 'web']);
        Permission::create(['name' => 'delete user', 'guard_name' => 'web']);

        $super_role = Role::create([
            'name' => 'Super Admin', 
            'guard_name' => 'web',
            'status' => 1
        ]);

        $super_role->givePermissionTo(Permission::all());

        $admin_role = Role::create([
            'name' => 'Admin', 
            'guard_name' => 'web',
            'status' => 1
        ]);

        $admin_role->givePermissionTo(Permission::all());

        $user = User::find(1);
        $user->assignRole($super_role);
    }
}
