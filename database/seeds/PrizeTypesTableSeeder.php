<?php

use Illuminate\Database\Seeder;

class PrizeTypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('prize_types')->insert([
            [
                'name' => '1 st'
            ],
            [
                'name' => '2 nd'
            ],
            [
                'name' => '3 rd'
            ],
            [
                'name' => 'Special'
            ],
            [
                'name' => 'Consolation'
            ]
        ]);
    }
}
