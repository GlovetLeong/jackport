@extends('layouts.app')
@section('header_styles')
    <link rel="stylesheet" href="{!! asset('css/plugins/chosen/bootstrap-chosen.css') !!}">
    <link rel="stylesheet" href="{!! asset('css/plugins/datapicker/datepicker3.css') !!}">

@endsection
@section('breadcrumb')
    @include('layouts.breadcrumb', [
        'title' => [
            'icon' => 'fa-list',
            'name' => 'Number',
        ],
        'links' => [
            [
                'name' => 'Number List',
                'url' => route('analytics.number')
            ],
        ]
    ])
@endsection

@section('content')
    <div class="wrapper wrapper-content animated fadeInRight">
        @include('layouts.notifications')
        <div class="row">
            <div class="col-lg-12">

                <div class="ibox float-e-margins border-bottom">
                    <div class="ibox-title">
                        <h5>Advance Search</h5>
                        <div class="ibox-tools">
                            <a class="collapse-link">
                                <i class="fa fa-chevron-down"></i>
                            </a>
                        </div>
                    </div>
                    <div class="ibox-content" style="display: none;">
                        <div class="row">
                            <form class="form-horizontal" method="GET">
                                <div class="col-sm-6">

                                    <div class="form-group">
                                        {!! Form::label('number', 'Number', ['class' => 'col-sm-2 control-label x_padding']) !!}
                                        <div class="col-sm-10">
                                            {!! Form::text('number', request('number'), ['class' => 'form-control']) !!}
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        {!! Form::label('jackpot_type_id', 'JackPot Type', ['class' => 'col-sm-2 control-label x_padding']) !!}
                                        <div class="col-sm-10">
                                            {{ Form::select('jackpot_type_id', 
                                            $jackpot_type_list,
                                            request('jackpot_type_id'), ['class' => 'form-control chosen-select jackpot_type_list']) }}
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        {!! Form::label('prize_type_id', 'Prize Type', ['class' => 'col-sm-2 control-label x_padding']) !!}
                                        <div class="col-sm-10">
                                            {{ Form::select('prize_type_id', 
                                            $prize_type_list,
                                            request('prize_type_id'), ['class' => 'form-control chosen-select prize_type_list']) }}
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="col-sm-4 col-sm-offset-2">
                                            <button class="btn btn-primary" type="submit">Search</button>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        {!! Form::label('status', 'Date', ['class' => 'col-sm-2 control-label x_padding']) !!}
                                        <div class="col-sm-10">
                                            <div class="date">
                                                <div class="input-daterange input-group" id="datepicker">
                                                    <input type="text" class="form-control" name="start_date" value="{{ !empty(request('start_date')) ? request('start_date') : '' }}"/>
                                                    <span class="input-group-addon">to</span>
                                                    <input type="text" class="form-control" name="end_date" value="{{ !empty(request('end_date')) ? request('end_date') : '' }}" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        {!! Form::label('limit', 'Limit', ['class' => 'col-sm-2 control-label x_padding']) !!}
                                        <div class="col-sm-10">
                                            {{ Form::select('limit', 
                                            [
                                                20 => '20', 
                                                100 => '100', 
                                                200 => '200'
                                            ], 
                                            request('limit'), ['class' => 'form-control chosen-select']) }}
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        {!! Form::label('order_by', 'Order By', ['class' => 'col-sm-2 control-label x_padding']) !!}
                                        <div class="col-sm-10">
                                            {{ Form::select('order_by', 
                                            [
                                                'desc' => 'Desc', 
                                                'asc' => 'Asc'
                                            ], 
                                            request('order_by'), ['class' => 'form-control chosen-select']) }}
                                        </div>
                                    </div>
                                </div>
                           </form>
                        </div>
                    </div>
                </div>

                <div class="ibox">
                    <div class="ibox-content" style="position: relative;">
                        <div class="table-responsive">
                            <table class="table table-striped">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Number</th>
                                        <th>Type</th>
                                        <th>Prize</th>
                                        <th>Date</th>
                                        <th>Last Drawn</th>
                                        <th>Drawn Days</th>
                                    </tr>
                                </thead>
                                @php
                                    $offset = !empty(request('page')) && request('page') != 1  ? 1 + (((request('page') - 1) * 20)) : 1;
                                @endphp
                                <tbody>
                                    @forelse ($number_list as $key => $row)
                                        <tr>
                                            <td>{{ $offset++ }}</td>
                                            <td>{{ $row->number }}</td>
                                            <td>
                                                <div class="{{ $row->getJackpotType()->name }}">
                                                    {{ $row->getJackpotType()->name }}
                                                </div>
                                            </td>
                                            <td>
                                                <div class="{{ $row->getPrizeType()->name }}">
                                                    {{ $row->getPrizeType()->name }}
                                                </div>
                                            </td>
                                            <td>{{ $row->date }}</td>
                                            <td>
                                                @if(empty(request('jackpot_type_id'))) 
                                                    <div>{{ $row->last_drawn_days }}</div>
                                                @else
                                                    <div>{{ $row->last_drawn_same_type_days }}</div>
                                                @endif
                                            </td>
                                            <td>{{ $row->drawn_days }}</td>
                                           
                                        </tr>
                                    @empty
                                        <tr>
                                            <td colspan="7" style="text-align: center;">No Result Found</td>
                                        </tr>
                                    @endforelse
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <td colspan="7">
                                            <div class="pull-right">
                                                {{ $number_list->appends([
                                                    'number' => request('number'),
                                                    'jackpot_type_id' => request('jackpot_type_id'),
                                                    'prize_type_id' => request('prize_type_id'),
                                                    'end_date' => request('end_date'),
                                                    'limit' => request('limit'),
                                                    'order_by' => request('order_by'),
                                                    ])->links() }}
                                                <div><b>Total:</b> {{ $number_total }}</div>
                                            </div>
                                        </td>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('footer_scripts')
    <script type="text/javascript" src="{!! asset('js/plugins/chosen/chosen.jquery.js') !!}"></script>
    <script type="text/javascript" src="{!! asset('js/plugins/datapicker/bootstrap-datepicker.js') !!}"></script>

    <script type="text/javascript" >
        //chosen dropdown select 
        $('.chosen-select').chosen({width: "100%"});

        //datepicker
          $(".date .input-daterange").datepicker({
                format: 'dd-mm-yyyy',
                keyboardNavigation: false,
                forceParse: false,
                autoclose: true
            });
            
        $(document).ready(function() {
         
        });
    </script>
@endsection