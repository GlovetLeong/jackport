<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>{{ config('constants.site.domain') }} | Admin Login</title>
    <link rel="stylesheet" href="{!! asset('css/vendor.css') !!}" />
    <link rel="stylesheet" href="{!! asset('css/app.css') !!}" />
</head>
    <body class="gray-bg">
        <div class="middle-box text-center loginscreen animated fadeInDown">
            <div>
                <div>
                    <h1 class="logo-name">4D</h1>
                </div>
                <h3>Welcome to {{ config('constants.site.domain') }}</h3>
                <form class="m-t" role="form" action="{{ url('login') }}" method="POST">
                    {{ csrf_field() }}
                    <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                        <input type="email" name="email" class="form-control" placeholder="Username" required="" value="{{ old('email') }}">
                        @if ($errors->has('email'))
                            <span class="help-block">
                                <strong>{{ $errors->first('email') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                        <input type="password" name="password" class="form-control" placeholder="Password" required="">
                        @if ($errors->has('password'))
                            <span class="help-block">
                                <strong>{{ $errors->first('password') }}</strong>
                            </span>
                        @endif
                    </div>
                    <button type="submit" class="btn btn-primary block full-width m-b">Login</button>
                </form>
                <p class="m-t"> <small>&copy; {{ date('Y') }} {{ config('constants.site.domain') }} ({{ config('constants.site.register_number') }}). All rights reserved</small> </p>
            </div>
        </div>
        <!-- Mainly scripts -->
        <script type="text/javascript" src="{!! asset('js/app.js') !!}"></script>
    </body>
</html>