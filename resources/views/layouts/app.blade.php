<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>{{ config('constants.site.domain') }} - @yield('title')</title>
    <link rel="icon" sizes="16x16" href="{!! asset('images/16x16_favicon.png') !!}" />
    @yield('header_styles')
    <link rel="stylesheet" href="{!! asset('css/vendor.css') !!}">
    <link rel="stylesheet" href="{!! asset('css/app.css') !!}">
    <link rel="stylesheet" href="{!! asset('css/editor.css') !!}">
</head>
<body>
    <!-- Wrapper-->
    <div id="wrapper">
        <!-- Navigation -->
        @include('layouts.navigation')
        <!-- Page wraper -->
        <div id="page-wrapper" class="gray-bg">
            <!-- Page wrapper -->
            @include('layouts.topnavbar')
            <!-- Breadcrumb -->
            @yield('breadcrumb')
            <!-- Main view  -->
            @yield('content')
            <!-- Footer -->
            @include('layouts.footer')
        </div>
        <!-- End page wrapper-->
    </div>
    <!-- End wrapper-->
    <script src="{!! asset('js/app.js') !!}" type="text/javascript"></script>
    @yield('footer_scripts')
</body>
</html>