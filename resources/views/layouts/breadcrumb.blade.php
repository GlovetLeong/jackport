@section('title', $title['name'])
@include('layouts.partials.breadcrumb', [
    'title' => $title,
    'links' => $links,
    'kol_profile_image' => !empty($kol_profile_image) ? $kol_profile_image : '',
    'kol_id' => !empty($kol_detail['kol_id']) ? $kol_detail['kol_id'] : '',
    'kol_name' => !empty($kol_detail['kol_name']) ? $kol_detail['kol_name'] : '',
    'status' => !empty($kol_detail['status']) ? $kol_detail['status'] : '',
])