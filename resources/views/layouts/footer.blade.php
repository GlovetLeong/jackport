<div class="footer">
    <div class="pull-right"></div>
    <div>
        <strong>Copyright</strong> {{ config('constants.site.title') }} &copy; {{ date('Y') }}  {{ config('constants.site.domain') }} ({{ config('constants.site.register_number') }}). All rights reserved
    </div>
</div>
