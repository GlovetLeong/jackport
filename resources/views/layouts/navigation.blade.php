<nav class="navbar-default navbar-static-side" role="navigation">
    <div class="sidebar-collapse">
        <ul class="nav metismenu" id="side-menu">

        <li class="nav-header">
                <div class="dropdown profile-element"> 
                    <a href="#" aria-expanded="false">
                        <span class="clear"> 
                            <span class="block m-t-xs"> 
                                <h4>{{ Auth::user()->name }}</h4>
                            </span> 
                            <span class="text-muted text-xs block">
                                {{ Auth::user()->getRoleName() }}
                            </span> 
                        </span>
                    </a>
                </div>
                <div class="logo-element">
                    O+
                </div>
            </li>

            {{-- Dashboard --}}
            <li{!! isActiveRoute('/', ' class="active"') !!}>
                <a href="{{ url('/') }}"><i class="fa fa-home"></i><span class="nav-label">Dashboard</span></a>
            </li>

            {{-- Number --}}
            <li{!! isActivePrefixNoDot('analytics', ' class="active"') !!}>
                <a href="javascript:void(0);"><i class="fa fa-line-chart"></i> <span class="nav-label">Analytics</span> <span
                                class="fa arrow"></span></a>
                <ul class="nav nav-second-level">
                    <li{!! isActiveRoute('analytics.number', ' class="active"') !!}><a href="{{ route('analytics.number') }}"><i
                                        class="fa fa-try"></i>Number</a></li>
                </ul>
            </li>

          
            {{-- Users --}}
            @if (auth()->user()->can('view user') || auth()->user()->can('add user') || auth()->user()->can('edit user') || auth()->user()->can('delete user'))
                <li{!! isActivePrefixNoDot('user', ' class="active"') !!}>
                    <a href="javascript:void(0);"><i class="fa fa-user"></i> <span class="nav-label">Users</span> <span
                                class="fa arrow"></span></a>
                    <ul class="nav nav-second-level">
                        <li{!! isActiveRoute('user', ' class="active"') !!}><a href="{{ route('user') }}"><i
                                        class="fa fa-list"></i>User List</a></li>
                        @if (auth()->user()->can('add user'))
                            <li{!! isActiveRoute('user.create', ' class="active"') !!}><a
                                        href="{{ route('user.create') }}"><i class="fa fa-plus"></i> Create User</a>
                            </li>
                        @endif
                    </ul>
                </li>
            @endif


        </ul>
    </div>
</nav>
