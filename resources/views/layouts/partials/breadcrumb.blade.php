<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2><i class="fa {{ $title['icon'] }}"></i> {{ $title['name'] }}</h2>
        <ol class="breadcrumb">
            <li>
                <a href="{{ url('/') }}">Dashboard</a>
            </li>
            @if (!empty($links))
                @foreach ($links as $link)
                    <li{!! ($loop->last) ? ' class="active"' : ''  !!}>
                        @if (!empty($link['url']))
                            <a href="{{ $link['url'] }}">
                                {!! $loop->last ? '<strong>'.$link['name'].'</strong>': $link['name'] !!}
                            </a>
                        @else
                            {!! $loop->last ? '<strong>'.$link['name'].'</strong>': $link['name'] !!}
                        @endif
                    </li>
                @endforeach
            @endif
        </ol>
    </div>
    <div class="col-lg-2">
        @if (!empty($kol_profile_image))
            <a {{ ($status == 1) ? 'target="_blank"' : '' }} href="{{ ($status == 1) ? env('FRONTEND_URL') . env('FRONTEND_URL_KOL_PROFILE_URL') . $kol_id : '#' }}">
                <img alt="image" class="img-circle img-responsive  center-block" width="60px" height="60px" style="padding-top: 15px" src="{{ $kol_profile_image }}">
            </a>
        @endif
    </div>
</div>               
