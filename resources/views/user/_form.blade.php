@section('header_styles')
    <link rel="stylesheet" href="{{ asset('css/plugins/switchery/switchery.css') }}">
    <link rel="stylesheet" href="{{ asset('css/plugins/steps/jquery.steps.css') }}">
    <link rel="stylesheet" href="{!! asset('css/plugins/chosen/bootstrap-chosen.css') !!}" >
@endsection

<h1>User Details</h1>
<fieldset>
    <div class="row">
        <div class="col-lg-8">
            <div class="form-group">
                {!! Form::label('name', 'User Name*', ['class' => 'asterisk']) !!}
                {!! Form::text('name', !empty($this_user->name) ? $this_user->name : null, ['class' => 'form-control']) !!}
            </div>
            <div class="form-group">
                {!! Form::label('email', 'User Email*', ['class' => 'asterisk']) !!}
                {!! Form::email('email', !empty($this_user->email) ? $this_user->email : null, ['class' => 'form-control']) !!}
            </div>
            <div class="form-group">
                {!! Form::label('user_role', 'Role') !!}
                {!! Form::select('user_role[]', $role_list, null, ['class' => 'form-control chosen-select role_list',]) !!}
            </div>
            <div class="form-group">
                {!! Form::label('password', 'Password*', [ 'class' => 'asterisk']) !!}
                {!! Form::password('password', ['id' => 'password', 'class' => 'form-control']) !!}
            </div>
            <div class="form-group">
                {!! Form::label('password_confirmation', 'Confirm Password*', ['class' => 'asterisk']) !!}
                {!! Form::password('password_confirmation', ['class' => 'form-control']) !!}
            </div>
            <div class="form-group">
                {!! Form::label('status', 'Status') !!}
                <div>
                    @php $status = isset($this_user->status) ? $this_user->status : 0; @endphp
                    {!! Form::checkbox('status', 1, ($status != 0) ? true : false,  ['class' => 'js-switch']) !!}
                </div>
            </div>
        </div>
        <div class="col-lg-4">
            <div class="text-center">
                <div style="margin-top: 20px">
                    <i class="fa fa-list" style="font-size: 180px;color: #e5e5e5 "></i>
                </div>
            </div>
        </div>
    </div>
</fieldset>

@php
    //get active tab number
    $active_tab = request('tab');
    $active_tab_number = 0;
    if ($active_tab == 'detail') {
        $active_tab_number = 0;
    }
    elseif ($active_tab == 'image') {
        $active_tab_number = 1;
    }
@endphp

@section('footer_scripts')
    <script type="text/javascript" src="{{ asset('js/plugins/jquery-validation/jquery.validate.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/plugins/jquery-validation/additional-methods.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/plugins/switchery/switchery.js') }}"></script>
    <script type="text/javascript" src="{!! asset('js/plugins/steps/jquery.steps.min.js') !!}"></script>
    <script type="text/javascript" src="{!! asset('js/plugins/select2/select2.full.min.js') !!}"></script>
    <script type="text/javascript" src="{!! asset('js/plugins/chosen/chosen.jquery.js') !!}"></script>
    <script type="text/javascript">
        $(document).ready(function(){

            var current_url = "{{ url()->current() }}";
           
            //step form
            $("#form-user").steps({
                bodyTag: "fieldset",
                labels: 
                {
                    finish: "{{ $submitButtonText }}",
                },
                enableAllSteps: true, 
                startIndex: {{ $active_tab_number }},
                onStepChanging: function (event, currentIndex, newIndex) {
                    if (currentIndex > newIndex) {
                        return true;
                    }
                    var form = $(this);
                    if (currentIndex < newIndex){
                        $(".body:eq(" + newIndex + ") label.error", form).remove();
                        $(".body:eq(" + newIndex + ") .error", form).removeClass("error");
                    }
                    form.validate().settings.ignore = ":hidden:not(.must)";
                    return form.valid();
                },
                onStepChanged: function (event, currentIndex, priorIndex) {
                    if (currentIndex == 0) {
                        window.history.replaceState('', '', current_url + "?tab=detial");    
                    }
                    else if (currentIndex == 1) {
                        window.history.replaceState('', '', current_url + "?tab=image");    
                    }
                },
                onFinishing: function (event, currentIndex) {
                    var form = $(this);
                    form.validate().settings.ignore = ":hidden:not(.must)";
                    return form.valid();
                },
                onFinished: function (event, currentIndex) {
                    var form = $(this);
                    form.submit();
                }
            }).validate({
                // ignore : '',
                rules: {
                    name: {
                        required: true
                    },
                    email: {
                        required: true,
                        email: true
                    },
                    password: {
                        required: {{ $mode == 'create' ? 'true' : 'false'}},
                        minlength: 6,
                        maxlength: 20
                    },
                    password_confirmation: {
                        equalTo: "#password"
                    }
                },
                errorPlacement: function (error, element) {
                    element.after(error);
                }
            });

            //chosen dropdown select 
            $('.chosen-select').chosen({width: "100%"});

            //switchery checkbox
            var elems = Array.prototype.slice.call(document.querySelectorAll('.js-switch'));
            elems.forEach(function(html) {
                var switchery = new Switchery(html, {
                    color: "#1AB394"
                });
            });
        });

        @if($mode == 'edit')
            //load edit kol data
            var this_user = $.parseJSON("{!! addslashes(json_encode($this_user)) !!}");
            //role
            var role = this_user.role_id;
            
            x = 0;
            $.each(role, function(){
                $(".role_list option[value=" + role[x].toString() + "]").attr("selected", true);
                x++;
            });
           
        @endif

    </script>
@endsection