@extends('layouts.app')
@section('breadcrumb')
    @include('layouts.breadcrumb', [
        'title' => [
            'icon' => 'fa-plus',
            'name' => 'Create User',
        ],
        'links' => [
            [
                'name' => 'User List',
                'url' => route('user')
            ],
            [
                'name' => 'Create User',
                'url' => route('user.create')
            ],
        ]
    ])
@endsection
@section('content')
    <div class="wrapper wrapper-content animated fadeInRight">
        @include('layouts.notifications')
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox">
                    <div class="ibox-content">
                        <div class="row">
                            <div class="col-md-12 form-horizontal">
                                {!! Form::open(['id' => 'form-user', 'method' => 'POST', 'url' => route('user.store')]) !!}
                                    @include('user._form', ['submitButtonText' => 'Add User', 'mode' => 'create'])
                                {!! Form::close() !!}
                            </div>	                              
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection