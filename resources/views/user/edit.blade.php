@extends('layouts.app')
@section('breadcrumb')
    @include('layouts.breadcrumb', [
        'title' => [
            'icon' => 'fa-user',
            'name' => 'Edit User',
        ],
        'links' => [
            [
                'name' => 'User List',
                'url' => route('user')
            ],
            [
                'name' => 'Edit User',
                'url' => route('user.edit', $this_user->id)
            ],
        ]
    ])
@endsection
@section('content')
    <div class="wrapper wrapper-content animated fadeInRight">
        @include('layouts.notifications')
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox">
                    <div class="ibox-content">
                        <div class="row">
                            <div class="col-md-12 form-horizontal">
                                {!! Form::model($this_user, ['id' => 'form-user', 'method' => 'PATCH', 'url' => route('user.update', $this_user->id)]) !!}
                                    {{ Form::hidden('id', $this_user->id) }}
                                    @include('user._form', ['submitButtonText' => 'Update User', 'mode' => 'edit'])
                                {!! Form::close() !!}
                            </div>	                              
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection