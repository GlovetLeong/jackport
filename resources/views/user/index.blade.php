@extends('layouts.app')
@section('header_styles')
    <link rel="stylesheet" href="{!! asset('css/plugins/chosen/bootstrap-chosen.css') !!}">
@endsection
@section('breadcrumb')
    @include('layouts.breadcrumb', [
        'title' => [
            'icon' => 'fa-list',
            'name' => 'Users',
        ],
        'links' => [
            [
                'name' => 'User List',
                'url' => route('user')
            ],
        ]
    ])
@endsection

@section('content')
    <div class="wrapper wrapper-content animated fadeInRight">
        @include('layouts.notifications')
        <div class="row">
            <div class="col-lg-12">

                <div class="ibox float-e-margins border-bottom">
                    <div class="ibox-title">
                        <h5>Advance Search</h5>
                        <div class="ibox-tools">
                            <a class="collapse-link">
                                <i class="fa fa-chevron-down"></i>
                            </a>
                        </div>
                    </div>
                    <div class="ibox-content" style="display: none;">
                        <div class="row">
                            <form class="form-horizontal" method="GET">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        {!! Form::label('name', 'User Name', ['class' => 'col-sm-2 control-label x_padding']) !!}
                                        <div class="col-sm-10">
                                            {!! Form::text('name', request('name'), ['class' => 'form-control']) !!}
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        {!! Form::label('email', 'User Email', ['class' => 'col-sm-2 control-label x_padding']) !!}
                                        <div class="col-sm-10">
                                            {!! Form::text('email', request('email'), ['class' => 'form-control']) !!}
                                        </div>
                                    </div>
                                    <div class="hr-line-dashed"></div>
                                   
                                    <div class="form-group">
                                        <div class="col-sm-4 col-sm-offset-2">
                                            <button class="btn btn-primary" type="submit">Search</button>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        {!! Form::label('status', 'Status', ['class' => 'col-sm-2 control-label x_padding']) !!}
                                        <div class="col-sm-10">
                                            {{ Form::select('status', 
                                            [
                                                '' => 'All', 
                                                1 => 'Active', 
                                                0 => 'Inactive'
                                            ], 
                                            request('status'), ['class' => 'form-control chosen-select']) }}
                                        </div>
                                    </div>
                                    <div class="hr-line-dashed"></div>
                                </div>
                           </form>
                        </div>
                    </div>
                </div>

                <div class="ibox">
                    <div class="ibox-content" style="position: relative;">
                        @if (auth()->user()->can('add user'))
                            <div class="row">
                                <div class="pull-right add-btn">
                                    <a href="{{ route('user.create') }}" class="btn btn-primary dim" type="button"><i class="fa fa-plus"></i></a>
                                </div>
                            </div>		
                        @endif
                        <div class="table-responsive">
                            <table class="table table-striped">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Name</th>
                                        <th>Email</th>
                                        <th>Role</th>
                                        <th>Status</th>
                                        <th>Date Created</th>
                                        <th>Actions</th>
                                    </tr>
                                </thead>
                                @php
                                    $offset = !empty(request('page')) && request('page') != 1  ? 1 + (((request('page') - 1) * 10)) : 1;
                                @endphp
                                <tbody>
                                    @forelse ($user_list as $key => $row)
                                        <tr>
                                            <td>{{ $offset++ }}</td>
                                            <td>
                                                <div class="col-lg-8 x_padding">
                                                    <span class="user_id hidden">{{ $row->id }}</span>
                                                    <span class="user_name">{{ $row->name }}</span>
                                                </div>
                                            </td>
                                            <td>{{ $row->email }}</td>
                                            <td>{{ $row->getRoleName() }}</td>
                                            <td>
                                                @if ($row->status == 1)
                                                    <span class="label label-primary">Active</span>
                                                @else
                                                    <span class="label label-danger">Deactive</span>
                                                @endif
                                            </td>
                                            <td>{{ $row->created_at }}</td>
                                            <td>
                                                @if (auth()->user()->can('edit user'))
                                                    <a href="{{ route('user.edit', $row->id) }}" class="btn btn-white btn-xs" title="Edit" value="">
                                                        <i class="fa fa-pencil"></i>
                                                    </a>
                                                @endif
                                            </td>
                                        </tr>
                                    @empty
                                        <tr>
                                            <td colspan="7" style="text-align: center;">No Result Found</td>
                                        </tr>
                                    @endforelse                                                            
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <td colspan="7">
                                            <ul class="pull-right">
                                                {{ $user_list->appends([
                                                    'name' => request('name'),
                                                    'email' => request('email'),
                                                    'status' => request('status'),
                                                    ])->links() }}
                                                <div><b>Total:</b> {{ $user_total }}</div>
                                            </ul>
                                        </td>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('footer_scripts')
    <script type="text/javascript" src="{!! asset('js/plugins/chosen/chosen.jquery.js') !!}"></script>
    <script type="text/javascript" >
        //chosen dropdown select 
        $('.chosen-select').chosen({width: "100%"});

        $(document).ready(function() {


        });
    </script>
@endsection