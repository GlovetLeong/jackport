<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Auth::routes();

Route::group(['namespace' => 'Auth'], function () {
    Route::get('login', ['as' => 'login', 'uses' => 'LoginController@showLoginForm']);
    Route::post('login', ['as' => 'login', 'uses' => 'LoginController@login']);
    Route::get('logout', 'LoginController@logout')->name('logout' );
});

Route::get('/', ['as' => 'dashboard', 'uses' => 'HomeController@index']);
Route::get('base', ['as' => 'curl', 'uses' => 'CurlController@base']);
Route::get('last', ['as' => 'curl', 'uses' => 'CurlController@last']);

Route::prefix('analytics')->group(function () {
	Route::get('number', ['as' => 'analytics.number', 'uses' => 'AnalyticController@number']);
});

Route::prefix('user')->group(function () {
	Route::get('/', ['as' => 'user', 'uses' => 'UserController@index']);
	Route::get('create', ['as' => 'user.create', 'uses' => 'UserController@create']);
	Route::post('create', ['as' => 'user.store', 'uses' => 'UserController@store']);
	Route::get('{user}/edit', ['as' => 'user.edit', 'uses' => 'UserController@edit']);
	Route::patch('{user}', ['as' => 'user.update', 'uses' => 'UserController@update']);
});


// Route::get('/home', 'HomeController@index')->name('home');

// Route::get('base', ['as' => 'curl', 'uses' => 'CurlController@base']);
// Route::get('row', ['as' => 'curl', 'uses' => 'CurlController@row']);



